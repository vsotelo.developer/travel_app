# travel_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Design Credits: [View](https://dribbble.com/shots/15057244-Travel-App/attachments/6784600?mode=media)

## Preview
[![APP_TRAVEL](https://img.youtube.com/vi/3CD088sd9iI/0.jpg)](https://www.youtube.com/shorts/3CD088sd9iI)