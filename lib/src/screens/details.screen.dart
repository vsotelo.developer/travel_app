import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailsScreen extends StatelessWidget {
   
  const DetailsScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Stack (
          children: [            
            const _BackgroundImage(),
            const Positioned(
              top: 40,
              left: 15,
              child: _IconArrowBack()
            ),            
            const Positioned(
              bottom: 0,
              child: SingleChildScrollView(child: _DetailLocation())
            ),
            Positioned(
              top: size.height * 0.38,
              left: size.width * 0.7,
              child: const _HeartIcon()
            ),            
          ],
        ),
      ),
    );
  }
}

class _IconArrowBack extends StatelessWidget {
  const _IconArrowBack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: const Icon(Icons.arrow_back_ios, size: 35, color: Colors.black)
    );
  }
}

class _BackgroundImage extends StatelessWidget {
  const _BackgroundImage({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SizedBox(
      width: double.infinity,
      height: size.height * 0.5,
      child: Image.asset('assets/img3.jpg', fit: BoxFit.fill, width: double.infinity),
    );
  }
}

class _HeartIcon extends StatelessWidget {
  const _HeartIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: const CircleAvatar(
        backgroundColor: Colors.white,    
        maxRadius: 32,  
        child: Padding(
          padding: EdgeInsets.only(top: 4),
          child: Text('❤️', style: TextStyle(fontSize: 25)),
        )
      ),
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow( color: Colors.black12, blurRadius: 9, offset: Offset(0, 2) )
        ]
      ),
    );
  }
}

class _DetailLocation extends StatelessWidget {
  const _DetailLocation({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    const double radius = 60;
    return Container (
      width: size.width,
      //height: size.height * 0.6,      
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(radius), topRight: Radius.circular(radius))
      ),
      child: Column(
        children: const [
          _Header(),
          _CarouselImagesDesc(),
          _BodyDescription(),
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 50, left: 20, right: 20),
      width: double.infinity,
      height: 70,
      color: Colors.white,
      child: Column (
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text('Mt Rinjani', style: GoogleFonts.notoSans(fontSize: 25, color: Colors.black, fontWeight: FontWeight.bold)),
                  Text('(356 Km)', style: GoogleFonts.notoSans(fontSize: 22, color: Colors.grey)),
                ],
              ),              
              Text('⭐ 4,5', style: GoogleFonts.notoSans(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
            ],
          ),
          Text('Mt Lombok, Indonesia', style: GoogleFonts.notoSans(fontSize: 17, color: Colors.grey, fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }
}

class _CarouselImagesDesc extends StatelessWidget {
  const _CarouselImagesDesc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 80,
      color: Colors.white,
      child: ListView(
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        children: const [
            _ImageDetail(asset: 'assets/img1.jpg', text: 'Waterfall'),
            _ImageDetail(asset: 'assets/img2.jpg', text: 'Lake'),
            _ImageDetail(asset: 'assets/img3.jpg', text: 'Village'),
            _ImageDetail(asset: 'assets/img1.jpg', text: 'Springs'),
            _ImageDetail(asset: 'assets/img2.jpg', text: 'Location'),
            _ImageDetail(asset: 'assets/img3.jpg', text: 'Gps'),
            _ImageDetail(asset: 'assets/img1.jpg', text: 'Welcome'),
        ],
      ),
    );
  }
}

class _ImageDetail extends StatelessWidget {
  const _ImageDetail({Key? key, required this.asset, required this.text}) : super(key: key);

  final String asset;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 50,
      margin: const EdgeInsets.only(left: 10, right: 10),
      color: Colors.white,
      child: Column(      
        children: [
          SizedBox(            
            width: 70,
            height: 60,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Image.asset(asset, fit: BoxFit.fill,)
            )
          ),
          const SizedBox(height: 2),
          Text(text, style: GoogleFonts.notoSans(fontSize: 12, color: Colors.grey, fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }
}

class _BodyDescription extends StatelessWidget {
  const _BodyDescription({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {    
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      color: Colors.white,
      child: Column(
        children: [
          SizedBox(
            width: 340,
            height: 175,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 2),
                Text('Description', style: GoogleFonts.notoSans(fontSize: 21, color: Colors.black, fontWeight: FontWeight.bold)),
                const SizedBox(height: 10),
                Text('Dolore fugiat enim aliqua commodo eu anim veniam aliqua amet ullamco laboris incididunt aliquip. Cupidatat officia voluptate sint do laboris do deserunt Lorem. Fugiat do esse excepteur enim excepteur irure. Laborum voluptate nisi proident id do laborum non esse anim.',
                 style: GoogleFonts.notoSans(fontSize: 13, color: Colors.black), textAlign: TextAlign.justify),
              ],
            ),
          ),            
          SizedBox(              
            width: 340,
            height: 75,
            child: MaterialButton(                    
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
              child: Text('Visit Place', style: GoogleFonts.notoSans(fontSize: 22, color: Colors.white, fontWeight: FontWeight.bold)),
              color: const Color(0xff009C77),
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}


