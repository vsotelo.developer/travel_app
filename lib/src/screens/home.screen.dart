import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_app/src/screens/details.screen.dart';

class HomeScreen extends StatelessWidget {
   
  const HomeScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,          
          child: Column(            
            children: [
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: const [
                    _LocationUser(),
                    _TitleHome(),                    
                  ],
                ),
              ),
              const _CarouselButtons(),
              const _CarouselLocations(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: const _BottonNavigation(),
    );
  }
}

class _BottonNavigation extends StatelessWidget {
  const _BottonNavigation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      elevation: 0,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      selectedItemColor: const Color(0xff009C77),
      backgroundColor: Colors.white,
      unselectedItemColor: Colors.grey,
      currentIndex: 0,
      items: const [
        BottomNavigationBarItem(
          icon: FaIcon(FontAwesomeIcons.home),
          label: '',
        ),
        BottomNavigationBarItem(
          icon:  FaIcon(FontAwesomeIcons.search),
          label: '',
        ),
        BottomNavigationBarItem(
          icon:  FaIcon(FontAwesomeIcons.user),
          label: '',
        ),
      ],
    );
  }
}

class _LocationUser extends StatelessWidget {
  const _LocationUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(      
      color: Colors.white,
      width: double.infinity,
      height: 60,
      child: Row(
        children: [
          const Icon(Icons.location_on_outlined, size: 20, color: Colors.grey),
          Text(' Lima, Perú', style: GoogleFonts.notoSans(fontSize: 15, color: Colors.grey)),
        ],
      ),
    );
  }
}

class _TitleHome extends StatelessWidget {
  const _TitleHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,      
      height: 120,
      child: Column (
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text('Where do you', style: GoogleFonts.notoSans(fontSize: 35, color: Colors.black, fontWeight: FontWeight.bold)),
          Text('like to go? 👋', style: GoogleFonts.notoSans(fontSize: 35, color: Colors.black, fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }
}

class _CarouselButtons extends StatelessWidget {
  const _CarouselButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: double.infinity,      
      height: 70,
      margin: const EdgeInsets.only(left: 10),
      child: ListView (
        physics: const BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        children: const [
          _ShortButtom(text: 'Place', active: false),
          _ShortButtom(text: 'Culinary', active: false),
          _ShortButtom(text: 'Culture', active: true),
          _ShortButtom(text: 'Deport', active: false),
          _ShortButtom(text: 'Aventure', active: false),
          _ShortButtom(text: 'Sleep', active: false)
        ],
      ),
    );
  }
}

class _CarouselLocations extends StatelessWidget {
  const _CarouselLocations({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        color: Colors.white,
        child: ListView (
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          children: const [
            _LocationImage(locationImage: 'assets/img2.jpg', textPrimary: 'Machu Picchu', textSecondary: 'Cuzco, Perú',),
            _LocationImage(locationImage: 'assets/img3.jpg', textPrimary: 'Montain 7 Colors', textSecondary: 'Cuzco, Perú',),
            _LocationImage(locationImage: 'assets/img1.jpg', textPrimary: 'Laguna Azul', textSecondary: 'Cuzco, Perú',),
          ],
        ),
      )
    );
  }
}

class _ShortButtom extends StatelessWidget {
  const _ShortButtom({Key? key, required this.text, required this.active}) : super(key: key);

  final String text;
  final bool active;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 110,
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: RawMaterialButton(
        elevation: 0,
        child: Text(text, style: GoogleFonts.notoSans(fontSize: 15, color: active ? Colors.white : const Color(0xff009C77), fontWeight: FontWeight.bold)),
        fillColor: active ? const Color(0xff009C77) : const Color(0xffC7EEDB),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(35)),
        onPressed: (){}
      ),
    );
  }
}

class _LocationImage extends StatelessWidget {
  const _LocationImage({Key? key, required this.locationImage, required this.textPrimary, required this.textSecondary}) : super(key: key);

  final String locationImage;
  final String textPrimary;
  final String textSecondary;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, _createRuta());
      },
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),      
        width: 280,
        height: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30)        
        ),
        child: ClipRRect(        
          borderRadius: BorderRadius.circular(30),
          child: Stack(
            //fit: StackFit.expand,        
            children: [
               Image.asset(locationImage, fit: BoxFit.fill, width: double.infinity),
               Positioned (
                 bottom: 0,
                 child: _LocationDescription(textPrimary: textPrimary, textSecondary: textSecondary)
               ),
            ],
          ),
        ),
      ),
    );
  }
}

class _LocationDescription extends StatelessWidget {
  const _LocationDescription({Key? key, required this.textPrimary, required this.textSecondary}) : super(key: key);

  final String textPrimary;
  final String textSecondary;

  @override
  Widget build(BuildContext context) {    
    final width = MediaQuery.of(context).size.width;
    const double symetricSize = 8;
    return Container(
      width: width * 0.67,
      height: 110,
      margin: const EdgeInsets.only(bottom: symetricSize, left: symetricSize, right: symetricSize),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30)        
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: Row (        
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(              
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(textPrimary, style: GoogleFonts.notoSans(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
                Text(textSecondary, style: GoogleFonts.notoSans(fontSize: 15, color: Colors.grey, fontWeight: FontWeight.bold)),
              ],
            ),            
            const Center(child: FaIcon(FontAwesomeIcons.heart, color: Colors.grey, size: 30))
          ],
        ),
      ),
    );
  }
}


  Route _createRuta() {
    return PageRouteBuilder(
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => const DetailsScreen(),
      transitionDuration: const Duration(seconds: 1),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
         
         final curveAnimation = CurvedAnimation(parent: animation, curve: Curves.easeInOut);

         return SlideTransition(
           position: Tween<Offset>(begin: const Offset(0.5, 1.0), end: Offset.zero).animate(curveAnimation),
           child: child,
         );
      },
    );
  }