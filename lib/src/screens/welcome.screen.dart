
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_app/src/screens/home.screen.dart';
import '../widgets/slideshow.widget.dart';

class WelcomeScreen extends StatelessWidget {
   
  const WelcomeScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          child: Column(
            children: const [
              SizedBox(height: 20),
              _Title(),
              Expanded(child: _MiShiledow()),
              _ButtonStarterd(),
            ],
          ),
        ),
      ),
    );
  }
}

class _MiShiledow extends StatelessWidget {
  const _MiShiledow({ Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SlideshowWidget(
      pointsTop: false,
      focusColor: const Color(0xff009C77),
      noFocusColor: const Color(0xffC7EEDB),
      bulletSecondary: 8,
      bulletPrimary: 10,
      slides: [
        SvgPicture.asset('assets/svg1.svg'),
        SvgPicture.asset('assets/svg2.svg'),
        SvgPicture.asset('assets/svg3.svg'),
      ],
    );
  }
}


class _Title extends StatelessWidget {
  const _Title({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 200,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
           Text('Get Cultural', style: GoogleFonts.notoSans(fontSize: 45, color: Colors.black, fontWeight: FontWeight.bold)),
           Text('Experience 🌳', style: GoogleFonts.notoSans(fontSize: 45, color: Colors.black, fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }
}

class _ButtonStarterd extends StatelessWidget {
  const _ButtonStarterd({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 130,
      color: Colors.white,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 29),
        child: MaterialButton(                    
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
          child: Text('Get Started', style: GoogleFonts.notoSans(fontSize: 22, color: Colors.white, fontWeight: FontWeight.bold)),
          color: const Color(0xff009C77),
          onPressed: () {
            Navigator.pushReplacement(context, _createRuta());
          },
        ),
      ),
    );
  }

  Route _createRuta() {
    return PageRouteBuilder(
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => const HomeScreen(),
      transitionDuration: const Duration(seconds: 1),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
         
         final curveAnimation = CurvedAnimation(parent: animation, curve: Curves.easeInOut);

         return SlideTransition(
           position: Tween<Offset>(begin: const Offset(0.5, 1.0), end: Offset.zero).animate(curveAnimation),
           child: child,
         );
      },
    );
  }
}